import React,{Component} from 'react'
import {Form,FormGroup,Button,Input} from 'reactstrap'
import { TiLocationArrow } from 'react-icons/ti';
import { FaMobileAlt } from 'react-icons/fa';
import emoji from './emoji.jpg';
import './style.css'
import Comments from './Comments'
class ReviewPage extends Component
{
    constructor(props)
    {
        super(props)
        this.state={
        name:'',
        comment:'',
        postId:'',
        email:''
        }
    }
    onNameChange=(e)=>{
        this.setState({
            name:e.target.value
        })
    }
    onCommentChange=(e)=>
    {
        this.setState({
            comment:e.target.value
        })
    }
    SubmitValue = (e) => {
    //     localStorage.setItem('commentdata',JSON.stringify(this.state));
        
    
    //   this.props.nameData(this.state.name)
    //     this.props.commentData(this.state.comment)
    //     this.setState({
    //         name:'',
    //         comment:''
    //     })

    fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    body: JSON.stringify({
        name: this.state.name,
      body: this.state.comment,
      postId:this.state.postId,
      email:this.state.email
    
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  })
  .then(response => response.json())
  .then(json => console.log(json))

        e.preventDefault()
     }
    

    

    render()
    {
    
        return(
            <div>
        <Form onSubmit={this.SubmitValue}>
            <FormGroup>
            <h1>Say Something about React</h1>
            </FormGroup>
            <FormGroup>
                <Input id="ip" type="text" value={this.state.name} isSubmitted={true} onChange={this.onNameChange} placeholder="Your Name"/>
                <img id="img" src={emoji} alt="emoji"/>
                </FormGroup>
                <FormGroup>
                <FaMobileAlt id="icon1"/>
                    <textarea value="Your Comment" value={this.state.comment} isSubmitted={true} onChange={this.onCommentChange}/>
                    </FormGroup>
                    <FormGroup>
                    <TiLocationArrow id="icon2"/>
        <Button id="btn">Comment</Button>
                        </FormGroup>
                        
            </Form>
            <Comments/>
            </div>
        )
    }
}
export default ReviewPage
